const DTP_API_PATH = '../../dtp-api/';
const PORT = require('../../dtp-ecosystem/reference/port');
const ENV = process.env.NODE_ENV;

module.exports = {
  server: {
    port: PORT['dtp-admin'][ENV],
    handlers: require('./system/handlers'),
    middlewares: require('./system/middlewares')
  },
  mongo: {
    db: 'DTP-Database',
    models: require(DTP_API_PATH + 'config/system/mongo-models'),
    strict: true,
    log: process.env.LOG_LEVEL || 'info'
  }
};