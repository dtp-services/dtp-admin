const $Ban = require('../modules/ban');
const $log = require('../libs/log');

module.exports = function (app) {
  app.post('/deletePost', (req, res) => {
    $Ban.deletePost(req.body)
      .then(() => {
        res.end('ok');
      })
      .catch((err) => {
        $log.error(err);
        res.end('Server Error');
      });
  });
};