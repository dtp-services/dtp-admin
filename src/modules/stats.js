const $mongo = require('../libs/mongo');
const $Promise = require('bluebird');

const Intervals = { // in sec
  online: 60,
  today: 60 * 60 * 24,
  lastWeek: 60 * 60 * 24 * 7
};

class Stats {
  static get db () {
    return {
      users: $mongo.collection('users'),
      posts: $mongo.collection('posts'),
      groupAccounts: $mongo.collection('groupAccounts')
    };
  }

  static async get() {
    return await $Promise.props({
      botUsers: Stats.getBotUsers(),
      apiUsers: Stats.getApiUsers(),
      posted: Stats.getPosted(),
      accounts: Stats.getAccounts()
    })
  }

  static async statsAggregate (statGroup) {
    const time = Math.round((new Date()).getTime() / 1000);
    const pipeline = [];
    const $facet = {};
    let timeField, db;

    switch (statGroup) {
      case 'botUsers':
        timeField = 'last_bot_session';
        db = Stats.db.users;
      break;
      case 'apiUsers': 
        timeField = 'last_api_session';
        db = Stats.db.users;
      break;
      case 'posted': 
        timeField = 'createdDate';
        db = Stats.db.posts;
      break;
      case 'accounts':
        timeField = 'createdDate';
        db = Stats.db.groupAccounts;
      break;
      defualt:
        throw new Error('unknown type: ' + statGroup);
      break;
    }

    Object.keys(Intervals).forEach((name) => {
      const interval = Intervals[name];
      const pipeline = [];
      const $match = {};

      $match[timeField] = {
        $gte: (time - interval)
      };

      pipeline.push({
        $match
      });

      pipeline.push({
        $count: 'count'
      });

      $facet[name] = pipeline;
    });

    pipeline.push({
      $facet
    });

    const [resIntervals] = await db.aggregate(pipeline);

    return Object.keys(resIntervals).reduce((res, intervalName) => {
      if (resIntervals[intervalName].length === 0) {
        res[intervalName] = 0;
        return res;
      }

      res[intervalName] = resIntervals[intervalName][0].count;

      return res;
    }, {});
  }

  static async getBotUsers () {
    return await Stats.statsAggregate('botUsers');
  }
  
  static async getApiUsers () {
    return await Stats.statsAggregate('apiUsers');
  }
  
  static async getPosted () {
    return await Stats.statsAggregate('posted');
  }
  
  static async getAccounts () {
    return await Stats.statsAggregate('accounts');
  }
  
}

module.exports = Stats;