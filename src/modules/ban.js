const $mongo = require('../libs/mongo');
const $log = require('../libs/log');

class Ban {
  static get db () {
    return {
      banList: $mongo.collection('banList'),
      posts: $mongo.collection('posts'),
      contentIndex: $mongo.collection('contentIndex'),
      groupAccounts: $mongo.collection('groupAccounts'),
      groupAccountsMembers: $mongo.collection('groupAccountsMembers'),
      users: $mongo.collection('users')
    };
  }

  static async deletePost (data) {
    const {
      reason,
      path,
      userFrom,
      blockAuthor
    } = data;

    const createdDate = Math.round( (new Date()).getTime() / 1000 );
    const insertData = {
      path,
      reason,
      userFrom,
      createdDate
    };

    let blockedAuthor;

    if (blockAuthor) {
      blockedAuthor = await Ban.blockAuthorByPath(path);

      if (!blockedAuthor) {
        throw new Error('Not found author by post path: ' + path);
      }

      insertData.blockAuthor = blockedAuthor;
    }

    const post = await Ban.db.posts.findOne({ shortId: path });

    if (!post) {
      throw new Error('not found post: ' + path);
    }

    await Ban.db.contentIndex.deleteMany({ shortId: post.shortId });
    await Ban.db.posts.deleteMany({ shortId: post.shortId });
    await Ban.db.banList.insert(insertData);
  }

  static async blockAuthorByPath (path) {
    const post = await Ban.db.posts.findOne({ shortId: path });

    if (!post) {
      return;
    }

    let userIds = [], accountId;

    if (post.account_id) {
      accountId = post.account_id;

      const accountOwners = await Ban.db.groupAccountsMembers.find({
        account_id: accountId,
        access: 2
      });

      userIds.push(...accountOwners.map((item) => item.user_id));

      await Ban.db.groupAccounts.deleteMany({
        id: accountId
      });
      await Ban.db.groupAccountsMembers.deleteMany({
        account_id: accountId
      });
    } else if (post.user_id) {
      userIds.push(post.user_id);
    }

    await Ban.db.users.editMany({
      id: {$in: userIds}
    }, {
      ban: true
    });

    return userIds;
  }
}

module.exports = Ban;